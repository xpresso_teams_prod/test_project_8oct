import os
import sys
import types
import numpy
import keras
import pickle
import pandas
import marshal
import seaborn
import autofeat
import warnings
import matplotlib
import tensorflow
import matplotlib.pyplot
from xgboost import XGBRegressor
from keras.models import Sequential
from sklearn.impute import SimpleImputer
from keras.layers import Dense, Dropout, LSTM
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from xpresso.ai.core.data.exploration import Explorer
from autofeat import FeatureSelector, AutoFeatRegressor
from xpresso.ai.core.data.automl import StructuredDataset
from xpresso.ai.core.data.visualization import Visualization
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler, RobustScaler, QuantileTransformer
from xpresso.ai.core.data.versioning.controller_factory import VersionControllerFactory
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_file = pickle.load(open(f"{PICKLE_PATH}/train_file.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    df_initial = pickle.load(open(f"{PICKLE_PATH}/df_initial.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    df_cleaned = pickle.load(open(f"{PICKLE_PATH}/df_cleaned.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    X_train_transformed = pickle.load(open(f"{PICKLE_PATH}/X_train_transformed.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    X_test_transformed = pickle.load(open(f"{PICKLE_PATH}/X_test_transformed.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    y_train = pickle.load(open(f"{PICKLE_PATH}/y_train.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    y_test = pickle.load(open(f"{PICKLE_PATH}/y_test.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = training
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["train_file", "df_initial", "df_cleaned","X_train_transformed","X_test_transformed", "y_train", "y_test"]
def LSTM_model(X_train_normalized, y_train, X_test_normalized, y_test, epochs):
    epochs = epochs
    # Defining a 2-layer stacked LSTM
    X_train_lstm = numpy.reshape(X_train_normalized,(X_train_normalized.shape[0],1,-1)) 
    print("Shape of X_train = "+str(X_train_lstm.shape))
    X_test_lstm = numpy.reshape(X_test_normalized,(X_test_normalized.shape[0],1,-1))
    print("Shape of X_train = "+str(X_test_lstm.shape))#
    model = Sequential()
    model.add(LSTM(32, activation='relu', input_shape=(1, X_train_normalized.shape[1]), return_sequences = True))
    model.add(LSTM(32, activation='relu', input_shape=(1, X_train_normalized.shape[1])))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mse')
    history = model.fit(X_train_lstm, y_train, epochs=epochs, validation_split=0.20)
    y_train_pred_lstm = model.predict(X_train_lstm)
    y_test_pred_lstm = model.predict(X_test_lstm)
    R2_test_lstm = r2_score(y_test_pred_lstm, y_test)
    R2_train_lstm = r2_score(y_train_pred_lstm,y_train)
    print("\nR2 Score on the test dataset with LSTM = " + str(R2_test_lstm))
    #model.save("Model/lstm_model")
    #reconstructed_model = keras.models.load_model("Model/lstm_model")
    return (history, model, R2_test_lstm, R2_train_lstm)
epochs=2
# Fitting LSTM model
print("2. Using LSTM:\n")
history, model, R2_test_lstm, R2_train_lstm = LSTM_model(X_train_transformed, y_train, X_test_transformed, y_test, epochs)
if not os.path.exists("/output"):
    os.makedirs("/output")
model.save(os.path.join("/output", 'saved_model.h5'))

try:
    pickle.dump(train_file, open(f"{PICKLE_PATH}/train_file.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(df_initial, open(f"{PICKLE_PATH}/df_initial.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(df_cleaned, open(f"{PICKLE_PATH}/df_cleaned.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(X_train_transformed, open(f"{PICKLE_PATH}/X_train_transformed.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(X_test_transformed, open(f"{PICKLE_PATH}/X_test_transformed.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(y_train, open(f"{PICKLE_PATH}/y_train.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(y_test, open(f"{PICKLE_PATH}/y_test.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

